#Check env variables
if [ $WS_PRODUCTNAME = "" ]; then
  echo empty productName
  exit 1
else
    echo productname $WS_PRODUCTNAME
    if [ $WS_PROJECTNAME = "" ]; then
      echo empty projectName
      exit 2
    else
        echo PROJECTNAME $WS_PROJECTNAME
        if  [ $WS_USERKEY = "" ] ; then
          echo empty userKey
          exit 3
        else
          echo userkey $WS_USERKEY
          if  [ $WS_APIKEY = "" ] ; then
            echo empty orgToken
            exit 4
          else
            echo orgtoken $WS_APIKEY
            #API Get Product Token
            DATA={"userKey":"$WS_USERKEY","orgToken":"$WS_APIKEY","requestType":"getAllProducts"}
            PATTERN="\"productName\":\"$WS_PRODUCTNAME\",\"productToken\":\"[0-9a-f]*\""
            PRODUCTTOKEN=$(curl --location --request POST "$API" --header "$HEADER" --data-raw "$DATA"| grep -oEi "$PATTERN" | cut -d ':' -f 3 | cut -d '"' -f 2)
            echo API Get Product Token
            echo productToken $PRODUCTTOKEN
            if [ "$PRODUCTTOKEN" = "" ]; then
              echo empty productToken 
              exit 5
            else
              #API Get Project Token
              DATA={"userKey":"$WS_USERKEY","orgToken":"$WS_APIKEY","productToken":"$PRODUCTTOKEN","requestType":"getAllProjects"}
              PATTERN="\"projectName\":\"$WS_PROJECTNAME\",\"projectToken\":\"[0-9a-f]*\""
              PROJECTTOKEN=$(curl --location --request POST "$API" --header "$HEADER" --data-raw "$DATA" | grep -oEi "$PATTERN" | cut -d ':' -f 3 | cut -d '"' -f 2)
              echo API Get Project Token
              echo projectToken $PROJECTTOKEN
              echo curl --location --request POST $API --header $HEADER --data-raw $DATA
              if [ "$PROJECTTOKEN" = "" ]; then
                echo empty projectToken
                exit 6
              else
                #Get Support Token To Check Upload State
                DATA={"userKey":"$WS_USERKEY","projectToken":"$PROJECTTOKEN","requestType":"getProjectVitals"}
                PATTERN="\"requestToken\":\"[0-9a-f]*\""
                echo Get Support Token To Check Upload State
                echo curl --location --request POST "$API" --header "$HEADER" --data-raw "$DATA" | grep -oEi "$PATTERN" | cut -d ':' -f 2 | cut -d '"' -f 2
                sleep 60
                SUPPORTTOKEN=$(curl --location --request POST "$API" --header "$HEADER" --data-raw "$DATA" | grep -oEi "$PATTERN" | cut -d ':' -f 2 | cut -d '"' -f 2)
                echo SupportToken $SUPPORTTOKEN
                if [ "$SUPPORTTOKEN" = "" ]; then
                  echo empty supportToken
                  exit 7
                else
                  #API To Check Upload State
                  DATA={"userKey":"$WS_USERKEY","orgToken":"$WS_APIKEY","requestType":"getRequestState","requestToken":"$SUPPORTTOKEN"}
                  PATTERN="\"requestState\":\"[A-Z_]*\""
                  echo API To Check Upload State
                  STATE=$(curl --location --request POST "$API" --header "$HEADER" --data-raw "$DATA" | grep -oEi "$PATTERN" | cut -d ':' -f 2 | cut -d '"' -f 2 )
                  COUNTER=0
                  while [ "$STATE" != "FINISHED" ] && [ $COUNTER -le "60" ]; do
                    sleep 10
                    COUNTER=$(($COUNTER+1))
                    STATE=$(curl --location --request POST "$API" --header "$HEADER" --data-raw "$DATA" | grep -oEi "$PATTERN" | cut -d ':' -f 2 | cut -d '"' -f 2 )
                    case $STATE in
                      UNKNOWN)
                        #State = unknow -> either orgToken or requestToken are invalid
                        echo Unknown error
                        break
                        ;;
                      IN_PROGRESS)
                        #State = IN_PROGRESS -> update is in progress
                        echo Still in progress
                        ;;
                      UPDATED)
                        #State = UPDATED ->  inventory has been modified yet alerts have not been calculated yet
                        echo Updating
                        ;;
                      FINISHED)
                        #State = FINISHED -> alerts have been calculated successfully
                        #Get Project Risk Report In Workspace
                        echo Upload Finished
                        break
                        ;;
                      FAILED)
                        #State = FAILED -> an error has occurred during the update process
                        echo Upload Failed
                        break
                        ;;
                    esac
                    done
                    if [ "$COUNTER" -gt "60" ] ; then
                      echo API Requests TimeOut, Please Check The WhiteSource Service Status
                      exit 8
                    else
                      echo Update State:$STATE
                      echo API To Download Project RiskReport
                      DATA={"userKey":"$WS_USERKEY","orgToken":"$WS_APIKEY","requestType":"getProjectRiskReport","projectToken":"$PROJECTTOKEN"}
                      curl --location --request POST "$API" --header "$HEADER" --data-raw "$DATA" -o ./$WS_PROJECTNAME.pdf
                      echo curl --location --request POST "$API" --header "$HEADER" --data-raw "$DATA" -o ./$WS_PROJECTNAME.pdf
                      Report="./whitesource/$WS_PROJECTNAME.pdf"
                      ReportSize=$(du -sb $Report | awk '{ print $1 }')
                      echo Risk Report Size : $ReportSize byte
                      if [ "$ReportSize" -le "1000" ] ; then
                        echo 'Fail To Download RiskReport, Please Check The WhiteSource Service Status'
                        exit 9
                      else 
                        echo Finished The Job
                        exit 0
                  fi
                fi
              fi
            fi
          fi
        fi
      fi
    fi
  fi